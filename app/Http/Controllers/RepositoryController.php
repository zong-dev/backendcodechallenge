<?php

namespace App\Http\Controllers;

use App\Models\Commit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RepositoryController extends Controller
{
    private $baseUrl;
    private $repo;


    public function __construct()
    {
        $this->baseUrl = 'https://api.github.com/repos/';
        $this->repo = 'nodejs/node';
    }


    /**
     * verify if repository ( $this->repo ) exists and get commits from github api
     * @return \Illuminate\Http\Response
     */
    public function getRepo()
    {
        $endpoint = $this->baseUrl . $this->repo;
        $request = Http::get($endpoint);

        if($request->successful()):

            $endpoint = $this->baseUrl . $this->repo . '/comments';
            $request = Http::acceptJson()->get($endpoint)->json();


                $collection = collect($request)->sortBy('commit.date')->take(25);

            
                foreach($collection as $commit){

                    $checkCommit =  Commit::where('id', $commit['id'])->first();

                    if($checkCommit == false) {

                        Commit::create([
                            'id' => $commit['id'],
                            'commit_id' => $commit['commit_id'],
                            'node_id' => $commit['node_id'],
                            'html_url' => $commit['html_url'],
                            'url' => $commit['url'],
                            'committer' => $commit['user']['login'],
                            // 'committer_email' => $commit['committer']['email'],
                            'body' => $commit['body'],
                            'date_updated' => now()->parse($commit['updated_at']) ,
                        ]);

                    }
                    
                }

            return redirect()->route('getCommits');
        endif;
    }


    /**
     * list all recent commits
     * @return \Illuminate\Http\Response
     */
    public function getCommits()
    {
    
        $commits = Commit::orderBy('committer', 'asc')->get()->take(25);
        return view('commits', [
            'commits' => $commits
        ]);
    }
}
