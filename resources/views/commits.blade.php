<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Commits</title>
    <link rel="stylesheet" href="/css/app.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<style>
    .has-integer{
        color: #E6F1F6;
    }
    
    .has-integer:hover{
        color: #4e7080;
    }
</style>
<body>


    <div class="container mt-5">
       <div class="row">
           <div class="col-md-6 offset-md-3">
                <h3 class="display-5 mt-10 mb-5">Commits</h3>

                <ul class="list-group">
                    @foreach ($commits as $commit)
                        <li class="list-group-item">
                            <a href="{{$commit->html_url}}" style="text-decoration: none;">
                                <div class="row py-2">
                                    <div class="col-9 text-dark">
                                        <p>ID: <span class="{{is_integer(substr($commit->commit_id, -1) ? 'has-integer' : '') }}">{{$commit->id}}</span> <br> Author: {{$commit->committer}}</p>
                                    </div>
                                    <div class="col-3 text-dark">Date: {{ now()->parse($commit->date_updated)->diffForHumans()}}</div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
           </div>


       </div>
    </div>
    
</body>
</html>