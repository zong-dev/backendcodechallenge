
## Setup

How to install and setup this application

- clone this repo 
- run the following commands below

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

 - create a database with the name (challenge) 
 - run 

```bash
php artisan migrate
```

```bash
php artisan serve
```
