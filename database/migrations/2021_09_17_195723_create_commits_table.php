<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcommits', function (Blueprint $table) {
            $table->integer('id')->primary()->unique();
            $table->string('html_url')->nullable();
            $table->string('url')->nullable();
            $table->text('node_id');
            $table->string('commit_id');
            $table->string('committer');
            // $table->string('committer_email');
            $table->text('body')->nullable();
            $table->string('date_updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commits');
    }
}
